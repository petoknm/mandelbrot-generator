#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include <QFutureWatcher>
#include <QGraphicsScene>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QTimer>
#include <QDebug>
#include <math.h>
#include <time.h>
#include "renderdialog.h"
#include "palettedialog.h"
#include <emmintrin.h>

typedef enum{
    DEFAULT,
    FROM_FILE
} PaletteType;

typedef struct{
    PaletteType type;
    QImage *img;
} MyPalette;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    static void calculateMandelbrot(QVector<double> &list);
    static double map(double x, double in_min, double in_max, double out_min, double out_max);

public slots:
    void getPointMouseClicked(int x, int y);
    void drawFractal();
    void startRendering();
    void saveFractal();
    void getPalette();

private slots:
    void on_pushButtonGenerate_clicked();
    void on_actionSave_parameters_triggered();
    void on_actionOpen_parameters_triggered();
    void on_actionExit_triggered();
    void on_actionRender_triggered();
    void on_actionPalette_triggered();

private:
    QRgb getColor(double val);
    void loadList(int width, int height);

    Ui::MainWindow *ui;
    QGraphicsScene *myScene;
    double xmin,xmax,ymin,ymax,length;
    u_int32_t width,height,maxIter;
    QVector<QVector<double> > list;
    QFutureWatcher<void> watcher;
    RenderDialog renderDialog;
    PaletteDialog paletteDialog;
    MyPalette  palette;
    QFile *settingsFile;
    QTimer timer;
    struct timespec time_start,time_end;
};

#endif // MAINWINDOW_H
