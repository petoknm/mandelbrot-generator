#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    myScene=new QGraphicsScene(this);
    ui->graphicsView->setScene(myScene);

    connect(ui->graphicsView,&MyGraphicsView::sendMousePoint,this,&MainWindow::getPointMouseClicked);

    connect(&watcher,SIGNAL(progressRangeChanged(int,int)),ui->progressBar,SLOT(setRange(int,int)));
    connect(&watcher,SIGNAL(progressValueChanged(int)),ui->progressBar,SLOT(setValue(int)));

    connect(&renderDialog,SIGNAL(accepted()),this,SLOT(startRendering()));
    connect(&paletteDialog,SIGNAL(accepted()),this,SLOT(getPalette()));

    palette.type=DEFAULT;
    settingsFile=new QFile("settings.conf");

}

void MainWindow::calculateMandelbrot(QVector<double> &list){

    if(list[2]<0.5){   // do not use SSE
        double  y=list[0];  // first element is the y-coordinate
        u_int16_t max_iter=(u_int16_t)round(list[1]);   // second value is the maximum number of iterations

        for(int i=3;i<list.size();i++){ // for every pixel in the line

            double x=list[i];
            u_int32_t iter=0;

            double re=x;    // x is the real part of the constant
            double im=y;    // y is the imaginary part of the constant

            while(iter<max_iter && re*re+im*im<4.0){
                double t=re;
                re=re*re-im*im+x;
                im=2.0*t*im+y;
                iter++;
            }

            list[i]=iter;   // rewrite the item with the result
        }
    }else{  // use SSE
        u_int16_t max_iter=(u_int16_t)round(list[1]);

        for(int i=3;i<list.size()-1;i+=2){

            u_int32_t iter=0;
            __m128d cRe=_mm_set_pd(list[i],list[i+1]);
            __m128d cIm=_mm_set_pd(list[0],list[0]);
            __m128d Re=cRe;
            __m128d Im=cIm;
            __m128d length;
            __m128d mConst4=_mm_set_pd(4.0,4.0);
            __m128d mConst2=_mm_set_pd(2.0,2.0);
            __m128d m1,m2,m3,tRe;

            length=_mm_mul_pd(Re,Re);
            m2=_mm_mul_pd(Im,Im);
            length=_mm_add_pd(length,m2);

            double comparisonRes[] __attribute__ ((aligned (16))) = {0.0,0.0};

            bool complete1=false;
            bool complete2=false;

            while(!complete1 || !complete2){
                m1=_mm_cmpgt_pd(length,mConst4);    // length>4.0
                _mm_store_pd(comparisonRes,m1);
                if(((comparisonRes[0]==0.0)||(complete1))&&((comparisonRes[1]==0.0)||(complete2))&&(iter<max_iter)){
                    tRe=Re;

                    Re=_mm_mul_pd(Re,Re);
                    m2=_mm_mul_pd(Im,Im);
                    Re=_mm_sub_pd(Re,m2);
                    Re=_mm_add_pd(Re,cRe);

                    Im=_mm_mul_pd(mConst2,Im);
                    Im=_mm_mul_pd(Im,tRe);
                    Im=_mm_add_pd(Im,cIm);

                    length=_mm_mul_pd(Re,Re);
                    m3=_mm_mul_pd(Im,Im);
                    length=_mm_add_pd(length,m3);

                    iter++;

                }else{
                    if(comparisonRes[0]!=0.0 && !complete1){
                        complete1=true;
                        list[i+1]=iter;
                    }
                    if(comparisonRes[1]!=0.0 && !complete2){
                        complete2=true;
                        list[i]=iter;
                    }
                    if(iter>=max_iter){
                        complete1=true;
                        list[i]=iter;
                        complete2=true;
                        list[i+1]=iter;
                    }
                }
            }
        }
    }
}

double MainWindow::map(double x, double in_min, double in_max, double out_min, double out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void MainWindow::getPointMouseClicked(int x, int y)
{
    ui->lineEditX->setText(QString::number(map(x,0,ui->graphicsView->width(),xmin,xmax),103,15));
    ui->lineEditY->setText(QString::number(map(y,0,ui->graphicsView->height(),ymin,ymax),103,15));
}

void MainWindow::loadList(int width,int height){
    xmin=ui->lineEditX->text().toDouble();
    ymin=ui->lineEditY->text().toDouble();
    length=pow(0.5,ui->doubleSpinBoxZoom->value());
    maxIter=ui->spinBoxMaxIter->value();

    xmin-=length/2.0;
    ymin-=length*height/(2.0*width);
    xmax=xmin+length;
    ymax=ymin+length*height/width;

    list.clear();

    for(int i=0;i<height;i++){
        QVector<double> sublist;
        sublist.append(map(i,0,height,ymin,ymax));
        sublist.append(maxIter);
        sublist.append((ui->actionUse_SSE2_extensions->isChecked())? 1.0:0.0);
        for(int j=0;j<width;j++){
            sublist.append(map(j,0,width,xmin,xmax));
        }
        list.append(sublist);
    }
}

void MainWindow::drawFractal()
{
    disconnect(&watcher,SIGNAL(finished()),this,SLOT(drawFractal()));
    clock_gettime(CLOCK_MONOTONIC, &time_end);
    double time_spent=(time_end.tv_sec - time_start.tv_sec)+(time_end.tv_nsec - time_start.tv_nsec)/1E9;
    ui->labelTime->setText("It took "+QString::number(time_spent)+" sec");
    QImage im(width,height,QImage::Format_RGB16);

    for(u_int32_t i=0;i<height;i++){
        for(u_int32_t j=0;j<width;j++){
            im.setPixel(j,i,getColor(1.0*list[i][j+2]/maxIter));
        }
    }
    list.clear();
    myScene->clear();
    myScene->addPixmap(QPixmap::fromImage(im));
    ui->graphicsView->setSceneRect(ui->graphicsView->contentsRect());
}

void MainWindow::startRendering()
{
    renderDialog.getResolution(width,height);

    loadList(width,height);

    clock_gettime(CLOCK_MONOTONIC, &time_start);
    watcher.setFuture(QtConcurrent::map(list,&MainWindow::calculateMandelbrot));
    connect(&watcher,SIGNAL(finished()),this,SLOT(saveFractal()));
}

void MainWindow::saveFractal()
{
    disconnect(&watcher,SIGNAL(finished()),this,SLOT(saveFractal()));
    clock_gettime(CLOCK_MONOTONIC, &time_end);
    double time_spent=(time_end.tv_sec - time_start.tv_sec)+(time_end.tv_nsec - time_start.tv_nsec)/1E9;
    ui->labelTime->setText("It took "+QString::number(time_spent)+" sec");
    QString filename=QFileDialog::getSaveFileName(this,"Save Image","","PNG (*.png);;JPG (*.jpg)");
    if(!filename.contains(".jpg") && !filename.contains(".png"))    filename+=".png";
    QImage img(width,height,QImage::Format_RGB16);
    for(u_int32_t i=0;i<height;i++){
        for(u_int32_t j=0;j<width;j++){
            img.setPixel(j,i,getColor(1.0*list[i][j+2]/maxIter));
        }
    }
    list.clear();
    img.save(filename);
    disconnect(&watcher,SIGNAL(finished()),this,SLOT(saveFractal()));
    connect(&watcher,SIGNAL(finished()),this,SLOT(drawFractal()));
}

void MainWindow::getPalette()
{
    QString filename=paletteDialog.getCurrentPaletteFilename();
    if(QFile::exists(filename)){
        palette.img=new QImage(filename);
        palette.type=FROM_FILE;
    }else{
        palette.type=DEFAULT;
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonGenerate_clicked()
{
    width=ui->graphicsView->width()-2;
    height=ui->graphicsView->height()-2;

    loadList(width,height);

    clock_gettime(CLOCK_MONOTONIC, &time_start);
    watcher.setFuture(QtConcurrent::map(list,&MainWindow::calculateMandelbrot));
    connect(&watcher,SIGNAL(finished()),this,SLOT(drawFractal()));

}

void MainWindow::on_actionSave_parameters_triggered()
{
    QString filename=QFileDialog::getSaveFileName(this,QString("Save parameters"),"","Parameter files (*.param)");
    if(!filename.contains(".param"))    filename+=".param";
    QFile file(filename);
    if(!file.open(QIODevice::ReadWrite)){
        QMessageBox::warning(this,"Error","File could not be opened");
        return;
    }
    QString data;
    data+=ui->lineEditX->text()+"\n";
    data+=ui->lineEditY->text()+"\n";
    data+=QString::number(ui->doubleSpinBoxZoom->value())+"\n";
    data+=QString::number(ui->spinBoxMaxIter->value())+"\n";
    file.write(data.toStdString().c_str(),data.size());
    file.close();

}

void MainWindow::on_actionOpen_parameters_triggered()
{
    QString filename=QFileDialog::getOpenFileName(this,QString("Open parameters"),"","Parameter files (*.param)");
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly)){
        QMessageBox::warning(this,"Error","File could not be opened");
        return;
    }
    QString line=file.readLine().replace("\n","").replace("\r","");
    ui->lineEditX->setText(line);
    line=file.readLine().replace("\n","").replace("\r","");
    ui->lineEditY->setText(line);
    line=file.readLine().replace("\n","").replace("\r","");
    ui->doubleSpinBoxZoom->setValue(line.toDouble());
    line=file.readLine().replace("\n","").replace("\r","");
    ui->spinBoxMaxIter->setValue(line.toInt());
    file.close();
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_actionRender_triggered()
{
    renderDialog.show();
}

QRgb MainWindow::getColor(double val)
{
    if(palette.type==DEFAULT){
        return qRgb(255*val,255*val,255*val);
    }else if(palette.type==FROM_FILE){
        int pix=val*(palette.img->width()-1);
        return palette.img->pixel(pix,0);
    }
    return qRgb(0,0,0);
}

void MainWindow::on_actionPalette_triggered()
{
    paletteDialog.show();
}
