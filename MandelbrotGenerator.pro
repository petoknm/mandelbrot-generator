#-------------------------------------------------
#
# Project created by QtCreator 2014-12-23T14:37:33
#
#-------------------------------------------------

QT       += core gui concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MandelbrotGenerator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mygraphicsview.cpp \
    renderdialog.cpp \
    palettedialog.cpp

HEADERS  += mainwindow.h \
    mygraphicsview.h \
    renderdialog.h \
    palettedialog.h

FORMS    += mainwindow.ui \
    renderdialog.ui \
    palettedialog.ui

QMAKE_CXXFLAGS += -msse2
