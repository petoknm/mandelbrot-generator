#include "renderdialog.h"
#include "ui_renderdialog.h"

RenderDialog::RenderDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RenderDialog)
{
    ui->setupUi(this);
}

RenderDialog::~RenderDialog()
{
    delete ui;
}

void RenderDialog::getResolution(u_int32_t &width, u_int32_t &height)
{
    width=ui->spinBoxWidth->value();
    height=ui->spinBoxHeight->value();
}
