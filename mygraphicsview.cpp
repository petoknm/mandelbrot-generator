#include "mygraphicsview.h"

MyGraphicsView::MyGraphicsView(QWidget *parent) :
    QGraphicsView(parent)
{
    scene = new QGraphicsScene();
    this->setScene(scene);
}

void MyGraphicsView::mousePressEvent(QMouseEvent * e){
    emit sendMousePoint(e->pos().x(),e->pos().y());
}
