#ifndef PALETTEDIALOG_H
#define PALETTEDIALOG_H

#include <QDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>

namespace Ui {
class PaletteDialog;
}

class PaletteDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PaletteDialog(QWidget *parent = 0);
    ~PaletteDialog();
    QString getCurrentPaletteFilename();


public slots:
    void on_selectedTextChanged(QString name);

private slots:
    void on_pushButtonOpenFile_clicked();
    void on_pushButtonAdd_clicked();
    void on_pushButtonRemove_clicked();


private:
    Ui::PaletteDialog *ui;
    QStringList strList;
    QGraphicsScene *scene;
};

#endif // PALETTEDIALOG_H
