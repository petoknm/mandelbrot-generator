#ifndef RENDERDIALOG_H
#define RENDERDIALOG_H

#include <QDialog>

namespace Ui {
class RenderDialog;
}

class RenderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RenderDialog(QWidget *parent = 0);
    ~RenderDialog();

    void getResolution(u_int32_t &width,u_int32_t &height);

private:
    Ui::RenderDialog *ui;
};

#endif // RENDERDIALOG_H
