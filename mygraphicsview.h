#ifndef MYGRAPHICSVIEW_H
#define MYGRAPHICSVIEW_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsEllipseItem>
#include <QMouseEvent>

class MyGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit MyGraphicsView(QWidget *parent = 0);

signals:
    void sendMousePoint(int x,int y);

public slots:
    void mousePressEvent(QMouseEvent * e);

private:
       QGraphicsScene * scene;
};

#endif // MYGRAPHICSVIEW_H
