#include "palettedialog.h"
#include "ui_palettedialog.h"

PaletteDialog::PaletteDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PaletteDialog)
{
    ui->setupUi(this);
    scene=new QGraphicsScene(this);

    ui->graphicsView->setScene(scene);

    strList.append("Palette 1");
    strList.append("Palette 2");
    strList.append("Palette 3");

    ui->listWidget->addItems(strList);
    connect(ui->listWidget,SIGNAL(currentTextChanged(QString)),this,SLOT(on_selectedTextChanged(QString)));
}

PaletteDialog::~PaletteDialog()
{
    delete ui;
}

QString PaletteDialog::getCurrentPaletteFilename()
{
    return strList.at(ui->listWidget->currentRow());
}

void PaletteDialog::on_pushButtonOpenFile_clicked()
{
    ui->lineEditFilename->setText(QFileDialog::getOpenFileName(this,"Open palette","","Images (*png *jpg)"));
}

void PaletteDialog::on_pushButtonAdd_clicked()
{
    if(QFile::exists(ui->lineEditFilename->text())){
        strList.append(ui->lineEditFilename->text());
        ui->lineEditFilename->setText("");
        ui->listWidget->clear();
        ui->listWidget->addItems(strList);
    }else{
        QMessageBox::warning(this,"No file","No such file exists! Please enter a valid filename!");
    }
}

void PaletteDialog::on_pushButtonRemove_clicked()
{
    strList.removeAt(ui->listWidget->currentRow());
    ui->listWidget->clear();
    ui->listWidget->addItems(strList);
}

void PaletteDialog::on_selectedTextChanged(QString name)
{
    if(QFile::exists(name)){
        QImage img(name);
        scene->clear();
        scene->addPixmap(QPixmap::fromImage(img.scaled(ui->graphicsView->width()-5,25)));
        scene->setSceneRect(scene->itemsBoundingRect());
    }
}


